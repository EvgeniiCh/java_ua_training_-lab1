import model.taxes.*;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.Timeout;

public class TestTax {
    private Tax tax;
    @Rule
    public Timeout time = new Timeout(1000);
    @Test
    public void testAbroadTax(){

        tax = new AbroadTransferIncomeTax();
        double res =tax.getSumTax(10.0);
        Assert.assertEquals(res,3.0D,0.0D);
    }

    @Test
    public void testRoyaltyTax(){

        tax = new RoyaltiesIncomeTax();
        double res =tax.getSumTax(10.0);
        Assert.assertEquals(res,0.5D,0.0D);
    }
    @Test
    public void testWorkTax(){

        tax = new WorkIncomeTax();
        double res =tax.getSumTax(10.0);
        Assert.assertEquals(res,2.0D,0.0D);
    }
    @Test
    public void testPropertyTax(){

        tax = new PropertySalesIncomeTax();
        double res =tax.getSumTax(10.0);
        Assert.assertEquals(res,1.2D,0.0D);
    }
}
