import model.incomes.*;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.Timeout;

public class TestIncome {
    private Income income;
    @Rule
    public Timeout time = new Timeout(1000);
    @Test
    public void testAbroadTax(){

        income = new AbroadTransferIncome(10);
        double res =income.getSumTax();
        Assert.assertEquals(res,3.0D,0.0D);
    }

    @Test
    public void testRoyaltyTax(){

        income = new RoyaltiesIncome(10);
        double res =income.getSumTax();
        Assert.assertEquals(res,0.5D,0.0D);
    }
    @Test
    public void testWorkIncome(){

        income = new WorkIncome(10);
        double res =income.getSumTax();
        Assert.assertEquals(res,2.0D,0.0D);
    }

    @Test
    public void testPropertyTax(){

        income = new PropertySalesIncome(10);
        double res =income.getSumTax();
        Assert.assertEquals(res,1.2D,0.0D);
    }
}
