package model.incomes;

import model.taxes.PropertySalesIncomeTax;

public class PropertySalesIncome extends Income {
    public PropertySalesIncome(int sum){
        super(sum);
        tax = new PropertySalesIncomeTax();
    }
}
