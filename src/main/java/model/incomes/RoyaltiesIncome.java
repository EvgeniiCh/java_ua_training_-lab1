package model.incomes;

import model.taxes.RoyaltiesIncomeTax;

public class RoyaltiesIncome extends Income{
    public RoyaltiesIncome(int sum){
        super(sum);
        tax = new RoyaltiesIncomeTax();
    }
}
