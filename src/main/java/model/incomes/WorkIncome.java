package model.incomes;

import model.taxes.WorkIncomeTax;

public class WorkIncome extends Income{
    public WorkIncome(int sum){
        super(sum);
        tax = new WorkIncomeTax();
    }
}
