package model.incomes;

import model.taxes.Tax;

public abstract class Income {
    protected double sum;
    protected Tax tax;
    public Income(int sum){
        this.sum=sum;
    }
    public double getSumTax(){
        return tax.getSumTax(sum);
    }
    public String taxToString(){
        return tax.toString();
    }
}
