package model.incomes;

import model.taxes.AbroadTransferIncomeTax;

public class AbroadTransferIncome extends Income {
    public AbroadTransferIncome(int sum){
        super(sum);
        tax = new AbroadTransferIncomeTax();
    }
}
