package model;

import model.incomes.*;

import java.util.ArrayList;

import static model.incomes.TextConstant.NUMBER_TAXES;
import static model.incomes.TextConstant.SUM_TAXES;

public enum Human {
    Vasia("Vasia",new WorkIncome(12000),new PropertySalesIncome(90000)),
    Anton("Anton",new WorkIncome(6000),new WorkIncome(3000)),
    Nikita("Nikita",new PropertySalesIncome(2000),new RoyaltiesIncome(2000),new AbroadTransferIncome(3000));

    public String getName() {
        return name;
    }

    private String name;
    private ArrayList<Income> incomeList = new ArrayList<Income>();
    Human (String name){
        this.name = name;
    }
    Human (String name,Income ... incomes){
        this.name = name;
        for(Income income : incomes){

            incomeList.add(income);
        }
    }
    public int getNumberTaxes(){
        return incomeList.size();
    }
    public double getSumTaxes(){
        double sum =0.0;
        for(Income income : incomeList){
            sum+= income.getSumTax();
        }
        return sum;
    }

    @Override
    public String toString() {

        StringBuilder concatString = new StringBuilder();
        concatString = concatString.append(this.name).append("\n");

        concatString = concatString.append(NUMBER_TAXES).append(getNumberTaxes()).append("\n");

        concatString = concatString.append(SUM_TAXES).append(getSumTaxes()).append("\n");

        return new String(concatString);
    }
}
