package model.taxes;

public abstract class Tax {
    public abstract double getSumTax( double income);
}
