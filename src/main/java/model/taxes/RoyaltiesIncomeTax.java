package model.taxes;

import static model.taxes.TaxConstant.ROYALTIES_INCOME_TAX_VALUE;

public class RoyaltiesIncomeTax extends Tax{
    @Override
    public double getSumTax( double income){
        return ROYALTIES_INCOME_TAX_VALUE *income;
    }
}
