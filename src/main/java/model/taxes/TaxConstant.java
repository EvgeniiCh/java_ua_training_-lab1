package model.taxes;

public interface TaxConstant {
    double WORK_INCOME_TAX_VALUE =0.2;
    double ROYALTIES_INCOME_TAX_VALUE = 0.05;
    double PROPERTY_SALES_INCOME_TAX_VALUE = 0.12;
    double ABROAD_TRANSFER_INCOME_TAX_VALUE = 0.3;
}
