package model.taxes;

import static model.taxes.TaxConstant.ABROAD_TRANSFER_INCOME_TAX_VALUE;

public class AbroadTransferIncomeTax extends Tax{
    @Override
    public double getSumTax( double income){
        return ABROAD_TRANSFER_INCOME_TAX_VALUE*income;
    }
}