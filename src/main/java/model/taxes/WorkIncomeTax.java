package model.taxes;

import static model.taxes.TaxConstant.WORK_INCOME_TAX_VALUE;

public class WorkIncomeTax extends Tax{
    @Override
    public double getSumTax( double income){
        return WORK_INCOME_TAX_VALUE*income;
    }

}
