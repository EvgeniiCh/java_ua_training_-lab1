package model.taxes;

import static model.taxes.TaxConstant.PROPERTY_SALES_INCOME_TAX_VALUE;

public class PropertySalesIncomeTax extends Tax {
    @Override
    public double getSumTax (double income){
        return PROPERTY_SALES_INCOME_TAX_VALUE*income;
    }
}
