package model;

public class Model {
    public String getAllHumans(){

        StringBuilder concatString = new StringBuilder();


        for (Human human : Human.values()){
            concatString =concatString.append(human.toString());
        }
        return new String(concatString);
    }
}
